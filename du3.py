# Vybral som príklad z mojej domácej úlohy číslo 3, 
# kde f(x) = x^3; a = 1, h = 2, pričom som za základ zobral
# spätnú diferenciu.

class Richardson:
	def __init__(self):
		self.f = "x^3"								# predpis funkcie
		self.a = 1									# a
		self.h = 2									# h
		self.n = 5									# pocet riadkov
		self.k = 1									# krok
		self.function_exponent  = 3
		self.h_values 			= list()
		self.F 					= [[] for i in range(self.n)]
		self.vysledky  = dict()

		self.print_declaration()
		self.fill_h_values()
		self.return_backward_diference()
		self.return_richardson_extrapolation()


	def print_declaration(self):
		print("Numerická derivácia a Richardsonová extrapolácia")
		print("Zadanie: ")
		print("f(x) = {}; a = {}, h = {}".format(self.f, self.a, self.h))
		print("S použitím spätnej diferencie.")
		print("---------------------------------")


	def fill_h_values(self):
		for i in range(5):
			if i == 0:
				val = 2
			else:
				val = float(self.h_values[len(self.h_values)- 1]) / 2
			self.h_values.append(val)
		# print(self.h_values)


	def choosen_function(self, val):
		return val ** 3


	def first_derivation_choosen_function(self, val):
		return self.function_exponent * (val ** (self.function_exponent - 1 * 1))


	def return_backward_diference(self):
		self.count = 0							# index pola do ktoreho aktualne uklada vysledky 
		for h in self.h_values:
			val = (self.choosen_function(self.a) - self.choosen_function(self.a - h)) / h
			# print("f({}) - f({}) / {} = {}".format(self.choosen_function(self.a), self.choosen_function(self.a - h), h, val))
			self.F[0].append(val)
			

	def second_derivation_choosen_function(self, val): 
		return 2 * self.function_exponent * (val ** (self.function_exponent - 2 * 1))


	def return_richardson_extrapolation(self):
		def return_result():
			for i in range(len(self.F[self.count]) - 1):
				val = self.F[self.count][i] + self.F[self.count][i + 1]
				self.F[self.count + 1].append(val)

		for i in range(self.n):
			return_result()
			self.count += 1


	def return_results(self):
		count1 = count2 = 0
		for i in range(len(self.F)):
			for j in range(len(self.F[count1])):
				if count1 == 0:
					self.vysledky["F{}{}".format(count2, count1)] = self.F[i][j]
					print("F{}{}".format(count2, count1), "=", self.vysledky["F{}{}".format(count2, count1)])
				else:
					parameter = "{}{}".format(count2, count1)

					exponent = 1 + count1 - 1
					citatel = self.vysledky["F{}{}".format(count2-1, count1-1)] - 2 ** exponent * self.vysledky["F{}{}".format(count2, count1-1)]
					menovatel = 1 - 2 ** exponent
					val = citatel / menovatel

					self.vysledky["F{}{}".format(count2, count1)] = val
					print("F{} = {}".format(parameter, val))
				count2 += 1
			print("---------------------------------------")
			count1 += 1
			count2 = count1

r = Richardson()
r.return_results()
# print(r.vysledky)
print("f'(a) = ", r.first_derivation_choosen_function(r.a))